﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Forking
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Kishor : Hello");
            System.Console.WriteLine("Bhavesh : Hi");
        }

        public static void Display()
        {
            System.Console.WriteLine("Mohit : Hello Fork");
        }

        public static void Test()
        {
            System.Console.WriteLine("Ravina: Hii");
        }
    }
}
